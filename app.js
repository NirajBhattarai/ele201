const express = require("express")
const path = require('path')
const app = express()
const cookieParser = require('cookie-parser')
const logger = require('./logger')

const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const newsRouter = require('./routes/newsRoutes')

app.use(express.static(path.join(__dirname, 'views')))
app.use(cookieParser())
app.use(express.json())
app.use('/api/v1/users',userRouter)
app.use('/api/v1/news', newsRouter)
app.use('/', viewRouter)

app.use((err, req, res, next) => {
    logger.error(err.message);
    res.status(500).send('Something went wrong');
})

module.exports = app