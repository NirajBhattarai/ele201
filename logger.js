const winston = require('winston');
require('winston-mongodb');

const logger = winston.createLogger({
    transports: [
        new winston.transports.MongoDB({
            db: 'mongodb://localhost:27017/epharmacist',
            collection: 'logs',
            level: 'error',
            options: { useUnifiedTopology: true },
        }),
    ],
});

module.exports = logger;